# AB Age Gate
AB Age Gate Drupal module.
It prevents showing of the site content before user confirm his age. 

Support Drupal 8, Drupal 9, Drupal 10.

# Installation.

Install as usual drupal module. 

After Install go to settings form - make basic configurations and save it.

Setting path: /admin/config/ab_age_gate

Clear Cache.

# Settings description.
* Age gate type
  - full date - requires day, month and year.
  - yes/no - only asks user if it is older than minimal age.
  
* Allowed age - minimal age for validation. Default is 18
* Terms of Consent and Private Policy display - leave as it is. // doesn't work
  in this iteration
* Terms of Consent page - url to Term and Condition page
* Private Policy page - Privacy Policy link
* Age gate logo image - Upload logo for your site instead of default.
* Background settings:
 - Image - Upload image instead of default.
 - Color - You can set color as gradient top/bottom or set same color for solid
   color for background.
 
 * Primary accent color - Color of text on popup.
 * Popup Background Color - background color for agegate popup.
 * Button text color - Text color for submit button.
 * Button background color - Background color for button.
 
 * Custom Font Family - Set font family for agegate page. f.e. Helvetica
 * Ignore Pages - set list of pages where Age gate will be disabled. f.e
   /privacy-policy. Use <front> for homepage.
 
 # Variable settings
 In language settings you can find default presets for basic languages. All
 text were imported from sitesupport agegate.
