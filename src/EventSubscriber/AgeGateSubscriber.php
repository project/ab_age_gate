<?php

namespace Drupal\ab_age_gate\EventSubscriber;

use Drupal\ab_age_gate\AgeGateStatistics;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Render\AttachmentsInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event subscriber for age gate functionality.
 */
class AgeGateSubscriber implements EventSubscriberInterface {

  /**
   * Current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $request;

  /**
   * Configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The age gate statistics service.
   *
   * @var \Drupal\ab_age_gate\AgeGateStatistics
   */
  protected $statisticsService;

  /**
   * The URL generator service.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * Constructs a new AgeGateSubscriber object.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory service.
   * @param \Drupal\ab_age_gate\AgeGateStatistics $statisticsService
   *   The age gate statistics service.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   The URL generator service.
   */
  public function __construct(RequestStack $request_stack, ConfigFactoryInterface $config_factory, AgeGateStatistics $statisticsService, UrlGeneratorInterface $url_generator) {
    $this->request = $request_stack->getCurrentRequest();
    $this->configFactory = $config_factory;
    $this->statisticsService = $statisticsService;
    $this->urlGenerator = $url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::RESPONSE][] = ['onResponse', 1000];

    return $events;
  }

  /**
   * Event callback for the RESPONSE event.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The response event object.
   */
  public function onResponse(ResponseEvent $event) {
    // Only act upon the master request and not sub-requests.

    if ($event->isMainRequest()) {

      $config = $this->configFactory->get('ab_age_gate.settings');

      // Process ignore pages settings.
      $ignore_pages = $config->get('ignore_pages') ?? '';
      $ignore_pages = explode(PHP_EOL, $ignore_pages);
      foreach ($ignore_pages as $ignore_page) {
        if (!empty($ignore_page)) {
          if ($ignore_page == '<front>') {
            $base_url = $this->urlGenerator->generateFromRoute('<front>', [], ['absolute' => TRUE]);
            if ($base_url === $this->request->getUri()) {
              return;
            }
          }
          if (str_contains($this->request->getUri(), trim($ignore_page))) {
            return;
          }
        }
      }

      // Early exit if admin pages.
      $excluded_path = [
        'admin',
        'node/add',
        'translations/add',
      ];
      foreach ($excluded_path as $path) {
        if (str_contains($this->request->getUri(), $path)) {
          return;
        }
      }

      $response = $event->getResponse();
      // Only act if the response is one that is able to have attachments.
      if ($response instanceof AttachmentsInterface) {
        // Get any existing attachments.
        $attachments = $response->getAttachments();
        // Set library and settings.
        $default_language = \Drupal::service('language.default')->get();
        $attachments['drupalSettings']['ageGateSettings'] = $config->getRawData();
        $attachments['drupalSettings']['ageGateSettings']['default_language'] = $default_language->getId();
        $attachments['library'][] = 'ab_age_gate/age_gate';
        // Set the updated array back on the object.
        $response->setAttachments($attachments);
      }
    }
  }

}
