<?php

namespace Drupal\ab_age_gate\Controller;

use Drupal\ab_age_gate\AgeGateStatistics;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller for managing statistics related to the age gate.
 */
class StatisticsController extends ControllerBase {

  /**
   * The age gate statistics service.
   *
   * @var \Drupal\ab_age_gate\AgeGateStatistics
   */
  protected $statisticService;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new Drupal\ab_age_gate\Controller\StatisticsController object.
   *
   * @param \Drupal\ab_age_gate\AgeGateStatistics $statisticService
   *   The age gate statistics service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   */
  public function __construct(AgeGateStatistics $statisticService, RequestStack $requestStack) {
    $this->statisticService = $statisticService;
    $this->requestStack = $requestStack;
  }

  /**
   * Creates a new instance of the StatisticsController.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container interface.
   *
   * @return static
   *   A new instance of the StatisticsController.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('ab_age_gate.statistics'),
      $container->get('request_stack'),
    );
  }

  /**
   * AJAX callback for selecting data.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response containing the selected data as JSON.
   */
  public function select() {
    $data = $this->statisticService->selectCurrentData();
    $response = new Response();
    $response->setContent(json_encode($data));
    return $response;
  }

  /**
   * AJAX callback for inserting data.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   *
   * @throws \Exception
   *   Thrown when an error occurs during data insertion.
   */
  public function insert() {
    $data = $this->requestStack->getCurrentRequest()->request->all();
    $this->statisticService->insertCurrentData($data['load'], $data['isMobile']);
    $response = new Response();
    return $response;
  }

  /**
   * AJAX callback for updating data.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   *
   * @throws \Exception
   *   Thrown when an error occurs during data update.
   */
  public function update() {
    $data = $this->requestStack->getCurrentRequest()->request->all();
    $this->statisticService->updateData($data['type'], $data['isMobile']);
    $response = new Response();
    return $response;
  }

}
