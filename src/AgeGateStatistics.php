<?php

namespace Drupal\ab_age_gate;

use Drupal\Core\Database\Connection;

/**
 * Provides functionality to manage statistics for the Age Gate module.
 *
 * @package Drupal\ab_age_gate\Services
 */
class AgeGateStatistics {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a new Drupal\ab_age_gate\AgeGateStatistics object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * Select data for current day.
   *
   * @return array|bool
   *   An array of data for the current day, or FALSE if no data is found.
   */
  public function selectCurrentData() {
    $current_day = date('dWY', time());

    $connection = $this->database;
    $select = $connection->select('ab_age_gate_statistics', 's')
      ->fields('s', [])
      ->condition('day_id', $current_day);
    return $select->execute()->fetchAssoc();
  }

  /**
   * Insert data for the current day.
   *
   * @param int $load
   *   The load value.
   * @param bool $isMobile
   *   Indicates if the request is from a mobile device.
   *
   * @throws \Exception
   *   Thrown if there was an error during the database insert operation.
   */
  public function insertCurrentData($load = 1, $isMobile = FALSE) {
    $current_day = date('dWY', time());
    $current_week = date('WY', time());
    $connection = $this->database;
    $query = $connection->insert('ab_age_gate_statistics')
      ->fields([
        'day_id' => $current_day,
        'week_id' => $current_week,
        'load' => $load,
        'success' => 0,
        'under18' => 0,
        'fail' => 0,
        'desktop' => !$isMobile ? 1 : 0,
        'mobile' => $isMobile ? 1 : 0,
      ]);
    $query->execute();
  }

  /**
   * Update data for the current day.
   *
   * @param string $type
   *   The type of update (success, under18, fail).
   * @param bool $isMobile
   *   Indicates if the request is from a mobile device.
   *
   * @throws \Exception
   *   Thrown if there was an error during the database update operation.
   */
  public function updateData($type = 'success', $isMobile = FALSE) {
    $current_day = date('dWY', time());
    $current_week = date('WY', time());
    $data = $this->selectCurrentData();

    $connection = $this->database;
    $select = $connection->update('ab_age_gate_statistics')
      ->fields([
        'day_id' => $current_day,
        'week_id' => $current_week,
        'load' => ++$data['load'],
        'success' => $type == 'success' ? ++$data['success'] : $data['success'],
        'under18' => $type == 'under18' ? ++$data['under18'] : $data['under18'],
        'fail' => $type == 'fail' ? ++$data['fail'] : $data['fail'],
        'desktop' => !$isMobile ? ++$data['desktop'] : $data['desktop'],
        'mobile' => $isMobile ? ++$data['mobile'] : $data['mobile'],
      ])
      ->condition('day_id', $current_day);
    $select->execute();
  }

}
