<?php

namespace Drupal\ab_age_gate\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\file\FileUsage\FileUsageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Configure age gate settings for this site.
 */
class AgeGateSettingsForm extends ConfigFormBase {

  /**
   * Config settings name.
   *
   * @var string
   */
  const SETTINGS = 'ab_age_gate.settings';

  /**
   * File manager service.
   *
   * @var \Drupal\file\FileUsage\FileUsageInterface
   */
  protected $fileUsage;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Url generator service.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  protected $path_resolver;

  protected $requestStack;

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory, FileUsageInterface $file_usage, EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager, FileUrlGeneratorInterface $file_url_generator, ExtensionPathResolver $path_resolver, RequestStack $requestStack) {
    parent::__construct($config_factory);
    $this->fileUsage = $file_usage;
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
    $this->fileUrlGenerator = $file_url_generator;
    $this->path_resolver = $path_resolver;
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('file.usage'),
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('file_url_generator'),
      $container->get('extension.path.resolver'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'age_gate_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(static::SETTINGS);

    $form['age_gate_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Age gate type:'),
      '#default_value' => $config->get('age_gate_type') ?? 1,
      '#options' => [
        1 => $this
          ->t('Full date'),
        2 => $this
          ->t('Yes/No'),
        3 => $this->t('Dynamical Year')
      ],
    ];

    $form['language_preselect'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('User Language pre-select'),
      '#default_value' => $config->get('language_preselect') ?? FALSE,
    ];
    $form['use_datalayer'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use GTM Datalayer for statistics'),
      '#default_value' => $config->get('use_datalayer') ?? TRUE,
    ];

    $form['age_restriction'] = [
      '#type' => 'number',
      '#title' => $this
        ->t('Allowed age'),
      '#default_value' => $config->get('age_restriction') ?? 18,
      '#mix' => 16,
      '#max' => 25,
      '#step' => 1,
    ];

    $form['age_gate_consent_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Terms of Consent and Private Policy display'),
      '#default_value' => $config->get('age_gate_consent_type') ?? 1,
      '#options' => [
        1 => $this
          ->t('URL\'s in footer text'),
        2 => $this
          ->t('Checkbox with URL\' in text'),
      ],
    ];

    if ($config->get('terms_of_consent_node')) {
      $terms_of_consent_node = $this->entityTypeManager->getStorage('node')
        ->load($config->get('terms_of_consent_node'));
    }

    $form['terms_of_consent_node'] = [
      '#title' => 'Terms of Consent page',
      '#type' => 'entity_autocomplete',
      '#target_type' => 'node',
      '#default_value' => $terms_of_consent_node ?? NULL,
      '#selection_handler' => 'default',
      '#selection_settings' => [
        'target_bundles' => ['article', 'page'],
      ],
    ];

    if ($config->get('private_policy_node')) {
      $private_policy_node = $this->entityTypeManager->getStorage('node')
        ->load($config->get('private_policy_node'));
    }

    $form['private_policy_node'] = [
      '#title' => 'Private Policy page',
      '#type' => 'entity_autocomplete',
      '#target_type' => 'node',
      '#default_value' => $private_policy_node ?? NULL,
      '#selection_handler' => 'default',
      '#selection_settings' => [
        'target_bundles' => ['article', 'page'],
      ],
    ];

    $form['age_gate_logo_img'] = [
      '#type' => 'managed_file',
      '#title' => t('Age gate logo image'),
      '#name' => 'age_gate_logo_img',
      '#default_value' => $config->get('age_gate_logo_img_file_id') != NULL ? $config->get('age_gate_logo_img_file_id') : NULL,
      '#upload_location' => 'public://age-gate',
      '#upload_validators' => [
        'file_validate_extensions' => ['gif png jpg jpeg svg'],
        'file_validate_size' => [25600000],
        'file_validate_is_image' => [],
      ],
      //'#progress_indicator' => 'bar',
      //'#progress_message' => $this->t('Please wait...'),
    ];

    $form['bg_settings'] = [
      '#type' => 'details',
      '#title' => t('Background settings'),
      '#open' => TRUE,
    ];

    $form['bg_settings']['bg_type'] = [
      '#type' => 'radios',
      '#title' => t('Background type'),
      '#options' => [
        'image' => 'Image',
        'color' => 'Color/Gradient',
      ],
      '#default_value' => $config->get('bg_type') ?? 'image',
    ];

    $form['bg_settings']['background_container'] = [
      '#type' => 'container',
      '#states' => [
        'invisible' => [
          ':input[name="bg_type"]' => ['value' => 'color']
        ]
      ]
    ];


    $form['bg_settings']['background_container']['age_gate_background_img'] = [
      '#type' => 'managed_file',
      '#title' => 'Age gate background image',
      '#name' => 'age_gate_background_img',
      '#default_value' => $config->get('background_image_file_id') != NULL ? [$config->get('background_image_file_id')] : NULL,
      '#upload_location' => 'public://age-gate',
      '#upload_validators' => [
        'file_validate_extensions' => ['gif png jpg jpeg svg'],
        'file_validate_size' => [45600000],
        'file_validate_is_image' => [],
      ],
    ];

    $form['bg_settings']['top_color'] = [
      '#type' => 'container',
      '#attributes' => [
        'style' => 'display: flex; align-items: center; gap: 20px;',
      ],
      '#states' => [
        'visible' => [
          ':input[name="bg_type"]' => ['value' => 'color']
        ]
      ]
    ];

    $top_bg_color = $config->get('top_bg_color') ?? "#636466";
    $form['bg_settings']['top_color']['top_bg_color'] = [
      '#type' => 'textfield',
      '#title' => $this
        ->t('Top Background color'),
      '#description' => t('Top color of background gradient. Use #XXXXXX format for color.'),
      '#default_value' => $top_bg_color,
    ];
    $form['bg_settings']['top_color']['top_bg_color_preview'] = [
      '#type' => 'container',
      '#attributes' => [
        'style' => 'width: 40px; height: 40px; border: 1px solid #000; background-color: ' . $top_bg_color,
      ],
    ];


    $form['bg_settings']['bottom_color'] = [
      '#type' => 'container',
      '#attributes' => [
        'style' => 'display: flex; align-items: center; gap: 20px;',
      ],
      '#states' => [
        'visible' => [
          ':input[name="bg_type"]' => ['value' => 'color']
        ]
      ]
    ];

    $bottom_bg_color = $config->get('bottom_bg_color') ?? "#636466";
    $form['bg_settings']['bottom_color']['bottom_bg_color'] = [
      '#type' => 'textfield',
      '#title' => $this
        ->t('Bottom Background color'),
      '#description' => t('Bottom color of background gradient. Use #XXXXXX format for color.'),
      '#default_value' => $bottom_bg_color,
    ];
    $form['bg_settings']['bottom_color']['bottom_bg_color_preview'] = [
      '#type' => 'container',
      '#attributes' => [
        'style' => 'width: 40px; height: 40px; border: 1px solid #000; background-color: ' . $bottom_bg_color,
      ],
    ];


    $form['primary_color'] = [
      '#type' => 'container',
      '#attributes' => [
        'style' => 'display: flex; align-items: center; gap: 20px;',
      ],
    ];

    $default_primary = $config->get('primary_accent_color') ?? "#636466";
    $form['primary_color']['primary_accent_color'] = [
      '#type' => 'textfield',
      '#title' => $this
        ->t('Primary accent color'),
      '#description' => t('Text color for agegate text. Use #XXXXXX format for color.'),
      '#default_value' => $default_primary,
    ];
    $form['primary_color']['primary_accent_color_preview'] = [
      '#type' => 'container',
      '#attributes' => [
        'style' => 'width: 40px; height: 40px; border: 1px solid #000; background-color: ' . $default_primary,
      ],
    ];

    $form['secondary_color'] = [
      '#type' => 'container',
      '#attributes' => [
        'style' => 'display: flex;  align-items: center; gap: 20px;',
      ],
    ];

    $default_secondary = $config->get('secondary_accent_color') ?? "#ffffff";
    $form['secondary_color']['secondary_accent_color'] = [
      '#type' => 'textfield',
      '#title' => $this
        ->t('Popup Background Color'),
      '#description' => t('Background color for agegate popup. Use #XXXXXX format for color.'),
      '#default_value' => $default_secondary,
    ];
    $form['secondary_color']['secondary_accent_color_preview'] = [
      '#type' => 'container',
      '#attributes' => [
        'style' => 'width: 40px; height: 40px; border: 1px solid #000; background-color: ' . $default_secondary,
      ],
    ];


    $form['button_color'] = [
      '#type' => 'container',
      '#attributes' => [
        'style' => 'display: flex;  align-items: center; gap: 20px;',
      ],
    ];
    $default_text_color = $config->get('button_text_color') ?? "#000";
    $form['button_color']['button_text_color'] = [
      '#type' => 'textfield',
      '#title' => $this
        ->t('Button text color'),
      '#description' => t('Button text color. Use #XXXXXX format for color.'),
      '#default_value' => $default_text_color,
    ];
    $form['button_color']['button_text_color_preview'] = [
      '#type' => 'container',
      '#attributes' => [
        'style' => 'width: 40px; height: 40px; border: 1px solid #000; background-color: ' . $default_text_color,
      ],
    ];

    $form['button_bg_color'] = [
      '#type' => 'container',
      '#attributes' => [
        'style' => 'display: flex;  align-items: center; gap: 20px;',
      ],
    ];
    $default_bg_color = $config->get('button_bg_color') ?? "#e7bb50";
    $form['button_bg_color']['button_bg_color'] = [
      '#type' => 'textfield',
      '#title' => $this
        ->t('Button background color'),
      '#description' => t('Button background color. Use #XXXXXX format for color.'),
      '#default_value' => $default_bg_color,
    ];

    $form['button_bg_color']['button_bg_color_preview'] = [
      '#type' => 'container',
      '#attributes' => [
        'style' => 'width: 40px; height: 40px; border: 1px solid #000; background-color: ' . $default_bg_color,
      ],
    ];

    $form['font_family'] = [
      '#type' => 'textfield',
      '#title' => $this
        ->t('Custom Font Family'),
      '#description' => t('Set Custom Font Family. Set f.e. Helvetica, sans-serif as it uses in CSS property.'),
      '#default_value' => $config->get('font_family') ?? NULL,
    ];

    $form['ignore_pages'] = [
      '#type' => 'textarea',
      '#title' => $this
        ->t('Ignore Pages'),
      '#description' => t("Set list of pages for disable Age-Gate on them. One path by string. Example /page-1. Use &lt;front&gt; for frontage."),
      '#default_value' => $config->get('ignore_pages') ?? NULL,
    ];

    $languages = $config->get('language_texts');

    $form['text_container'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];

    // Display only active site language in form.
    $langcodes_on_site = array_keys($this->languageManager->getLanguages());
    $site_available_languages = [];

    // Checking if site has language with locale e.g. FR-BE or standalone language e.g. FR.
    foreach ($langcodes_on_site as $langcode_on_site) {
      // If site has LOCALE-LANG e.g. FR-BE format simply getting it from config.
      if (array_key_exists($langcode_on_site, $languages['languages'])) {
        $site_available_languages[$langcode_on_site] = $languages['languages'][$langcode_on_site];
      }
      // If site has single lang formal e.g. FR converting it to LOCALE-LANG international format.
      elseif (array_key_exists($langcode_on_site . '-' . $langcode_on_site, $languages['languages'])) {
        $site_available_languages[$langcode_on_site] = $languages['languages'][$langcode_on_site . '-' . $langcode_on_site];
      }
      if (!array_key_exists($langcode_on_site, $languages['languages'])) {
        if ($langcode_on_site == 'en') {
          $site_available_languages[$langcode_on_site] = $languages['languages']['ie' . '-' . $langcode_on_site];
        }
      }
    }

    foreach ($site_available_languages as $lang_code => $text) {
      $form['text_container']['languages'][$lang_code] = [
        '#type' => 'details',
        '#title' => $lang_code,
        '#description' => 'Available tokens: [min_age], [url_privacy_policy], [current_year]',
        '#open' => FALSE,
        '#tree' => TRUE,
      ];

      if (is_array($text)) {
        foreach ($text as $field_name => $field_text) {
          $form['text_container']['languages'][$lang_code][$field_name] = [
            '#type' => 'textfield',
            '#title' => $field_name,
            '#default_value' => $field_text,
            '#maxlength' => 350,
            '#tree' => TRUE,
          ];
        }
      }
      else {
        $file = file_get_contents('/var/www/html/web/modules/contrib/ab_age_gate/config/install/ab_age_gate.settings.yml');
        $yml = Yaml::decode($file);

        if ($lang_code == 'en') {
          $full_lang_code = 'ie' . '-' . $lang_code;
        }

        foreach ($yml['language_texts']['languages'][$full_lang_code] as $field_name => $field_text) {
          $form['text_container']['languages'][$lang_code][$field_name] = [
            '#type' => 'textfield',
            '#title' => $field_name,
            '#default_value' => $field_text,
            '#maxlength' => 350,
            '#tree' => TRUE,
          ];
        }
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $file = $form_state->getValue('age_gate_logo_img');

    if ($file_id = $form_state->getValue('age_gate_logo_img')) {
      $file = $this->entityTypeManager->getStorage('file')->load($file_id[0]);
      $file->setPermanent();
      $file->save();
      $logo_image_uri = $this->fileUrlGenerator->generateString($file->getFileUri());
      $this->config(static::SETTINGS)
        ->set('age_gate_logo_img', $logo_image_uri)->save();
      $this->config(static::SETTINGS)
        ->set('age_gate_logo_img_file_id', $file_id)->save();
    }
    else {
      $module_path = $this->path_resolver
        ->getPath('module', 'ab_age_gate');
      $host = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost();
      $default_logo = $host . '/' . $module_path . '/assets/images/default_logo.png';
      $this->config(static::SETTINGS)
        ->set('age_gate_logo_img', $default_logo)->save();
      $this->config(static::SETTINGS)
        ->set('age_gate_logo_img_file_id', NULL)->save();
    }

    if ($file_id = $form_state->getValue(['age_gate_background_img', '0'])) {

      $file = $this->entityTypeManager->getStorage('file')->load($file_id);;
      $file->setPermanent();
      $file->save();
      $background_image_uri = $this->fileUrlGenerator->generateString($file->getFileUri());
      $this->config(static::SETTINGS)
        ->set('age_gate_background_img', $background_image_uri)->save();
      $this->config(static::SETTINGS)
        ->set('background_image_file_id', $file_id)->save();
    }
    else {
      $module_path = $this->path_resolver
        ->getPath('module', 'ab_age_gate');
      $host = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost();
      $default_bg = $host . '/' . $module_path . '/assets/images/default-agegate-bg.png';
      $this->config(static::SETTINGS)
        ->set('age_gate_background_img', $default_bg)->save();
    }

    $this->config(static::SETTINGS)
      ->set('age_gate_type', $form_state->getValue('age_gate_type'))
      ->set('age_restriction', $form_state->getValue('age_restriction'))
      ->set('age_gate_consent_type', $form_state->getValue('age_gate_consent_type'))
      ->set('terms_of_consent_node', $form_state->getValue('terms_of_consent_node'))
      ->set('private_policy_node', $form_state->getValue('private_policy_node'))
      ->set('primary_accent_color', $form_state->getValue('primary_accent_color'))
      ->set('secondary_accent_color', $form_state->getValue('secondary_accent_color'))
      ->set('language_preselect', $form_state->getValue('language_preselect'))
      ->set('use_datalayer', $form_state->getValue('use_datalayer'))

      ->set('button_bg_color', $form_state->getValue('button_bg_color'))
      ->set('button_text_color', $form_state->getValue('button_text_color'))

      ->set('top_bg_color', $form_state->getValue('top_bg_color'))
      ->set('bottom_bg_color', $form_state->getValue('bottom_bg_color'))

      ->set('ignore_pages', $form_state->getValue('ignore_pages'))
      ->set('font_family', $form_state->getValue('font_family'))

      ->set('bg_type', $form_state->getValue('bg_type'))

      ->save();

    // Replacing [min_age], [url_privacy_policy], [current_year] placeholders with real data.
    $languages = $form_state->getValue('text_container');

    foreach (reset($languages) as $lang_code => $lang_fields) {
      $languages['languages'][$lang_code] = str_replace([
        '[min_age]',
        '[url_privacy_policy]',
        '[current_year]',
      ], [
        $form_state->getValue('age_restriction') ?? '[min_age]',
        $form_state->getValue('private_policy_node') != NULL ? '/node/' . $form_state->getValue('private_policy_node') : '[url_privacy_policy]',
        date("Y"),
      ], $lang_fields);
    }

    //TODO: save config only for needed languages.
    $this->config(static::SETTINGS)
      ->set('language_texts', $languages)
      ->save();

    parent::submitForm($form, $form_state);
  }

}
