'use strict';

(function ($, Drupal, cookies, drupalSettings) {
  Drupal.behaviors.ageGate = {
    attach: function (context, settings) {
      var useDatalayer = false;
      let cookie = Cookies.get('agegate');
      if (cookie) {
        return;
      }

      const elements = once('ageGate', 'body', context);
      elements.forEach(function () {
        var dataLayer = window.dataLayer || [];
        let langPreselect = drupalSettings.ageGateSettings.language_preselect ?? false;
        useDatalayer = drupalSettings.ageGateSettings.use_datalayer ?? false;
        var languages = [];
        var defaultLanguage = '';
        var hideStep2Class = '';
        if (langPreselect) {
          let allLanguages = drupalSettings.ageGateSettings.language_texts.languages;
          defaultLanguage = drupalSettings.ageGateSettings.default_language;
          languages = Object.keys(allLanguages);
          hideStep2Class = 'hidden';
        }
        const ageGateType = drupalSettings.ageGateSettings.age_gate_type;
        let hideButtonsClass = ageGateType === '1' ? 'hidden' : '';
        let hideInputsClass = ageGateType === '2' ? 'hidden' : '';
        let hideDynamicalClass = ageGateType !== '3' ? 'hidden' : '';

        let lang = document.documentElement.lang;
        let currentLang = drupalSettings.ageGateSettings.language_texts.languages[lang] ? lang : 'en';
        let isMobile = Math.min(window.screen.width) < 768 || navigator.userAgent.indexOf('Mobi') > -1;
        let fontColor = drupalSettings.ageGateSettings.primary_accent_color;
        let bgColor = drupalSettings.ageGateSettings.secondary_accent_color;
        let buttonTextColor = drupalSettings.ageGateSettings.button_text_color;
        let buttonBgColor = drupalSettings.ageGateSettings.button_bg_color;
        let bgType = drupalSettings.ageGateSettings.bg_type;
        let bgSettings = '';
        if (bgType === 'color') {
          bgSettings = 'background-image: linear-gradient(' + drupalSettings.ageGateSettings.top_bg_color + ', ' + drupalSettings.ageGateSettings.bottom_bg_color + ')';
        } else {
          bgSettings = 'background-image: url(\'' + drupalSettings.ageGateSettings.age_gate_background_img + '\');';
        }
        let html = '';

        html += '<div id="age-overlay" style="' + bgSettings + '; font-family: ' + drupalSettings.ageGateSettings.font_family + '"> '
          + '<div class="overlay-content" style="background-color: ' + bgColor + '">'
          + '<div class="overlay-logo-container" style="background-image: url('
          + drupalSettings.ageGateSettings.age_gate_logo_img + ')">'
          + '</div>';

        if (langPreselect) {
          html += '<div class="overlay-header-text step-1">';
          html += '<span style="color: ' + fontColor + '" class="text-block agegate-header step1-header">' + Drupal.t('Select your language') + '</span>';
          html += '<ul class="select-lang">';
          languages.map(function (lang) {
            html += `<li class="${lang}"><a href="#" class="ag-lang-select" data-lang="${lang}">${lang}</a></li>`;
          });
          html += '</ul>';
          html += '<button type="submit" style="background-color: ' + buttonBgColor + '; color: ' + buttonTextColor + '" class="step1-submit agegate-submit ' + hideInputsClass + '" tabindex="6">' + drupalSettings.ageGateSettings.language_texts.languages[currentLang].button_continue + '</button>';
          html += '</div>';
        }

        html += '<div class="overlay-header-text step-2 ' + hideStep2Class + '">';

        html += '<span class="text-block agegate-header">'
          + drupalSettings.ageGateSettings.language_texts.languages[currentLang].header
          + '</span>'
          + '<span class="text-block agegate-subheader ' + hideInputsClass + '">'
          + drupalSettings.ageGateSettings.language_texts.languages[currentLang].subheader
          + '</span>'
          + '</div>'; // End of overlay-header-text
        html += '<form class="input-container ' + hideStep2Class + '">';
        if (ageGateType === '1') {
          html += ''
            + '<div class="input-wrapper ' + hideInputsClass + '">'
            + '<input type="number" maxlength="2" class="day" min="0" max="31" placeholder="' + drupalSettings.ageGateSettings.language_texts.languages[currentLang].placeholder_d + '">'
            + '<input type="number" maxlength="2" class="month" min="0" max="12" placeholder="' + drupalSettings.ageGateSettings.language_texts.languages[currentLang].placeholder_m + '">'
            + '<input type="number" maxlength="4" class="year" min="0" max="2100" placeholder="' + drupalSettings.ageGateSettings.language_texts.languages[currentLang].placeholder_y + '">'
            + '</div>';
        }
        if (ageGateType === '3') {
          html += ''
            + '<div class="dynamical-wrapper input-wrapper ' + hideDynamicalClass + '">'
            + '<input type="number" maxlength="4" class="year" min="0" max="2100" placeholder="' + drupalSettings.ageGateSettings.language_texts.languages[currentLang].placeholder_y + '">'
            + '<input type="number"  maxlength="2" class="month visually-hidden" min="0" max="12" placeholder="' + drupalSettings.ageGateSettings.language_texts.languages[currentLang].placeholder_m + '">'
            + '<input type="number" maxlength="2" class="day visually-hidden" min="0" max="31" placeholder="' + drupalSettings.ageGateSettings.language_texts.languages[currentLang].placeholder_d + '">'
            + '</div>';
        }

        if (ageGateType === '2') {
          html += ''
            + '<div class="buttons-wrapper ' + hideButtonsClass + '">'
            + '<input type="button" class="yes" value="' + Drupal.t('Yes') + '" style="background-color: ' + buttonBgColor + '; color: ' + buttonTextColor + '">'
            + '<input type="button" class="no" value="' + Drupal.t('No') + '"  style="background-color: ' + buttonTextColor + '; color: ' + buttonBgColor + '">'
            + '</div>';
        }

        html += ''
          + '<div class="error-wrapper">'
          + '<div class="agegate-error agegate-error-invalid-dob ' + hideInputsClass + '">' + drupalSettings.ageGateSettings.language_texts.languages[currentLang].error_invalid_dob + '</div>'
          + '<div class="agegate-error agegate-error-too-young">' + drupalSettings.ageGateSettings.language_texts.languages[currentLang].error_too_young + '</div>'
          + '</div>'
          + '<div class="checkbox-item agegate-remember ' + hideInputsClass + '">'
          + '<input type="checkbox" name="agegate-remember-item" class="form-checkbox required form-check-input"/>'
          + '<label for="agegate-remember-item">'
          + drupalSettings.ageGateSettings.language_texts.languages[currentLang].remember_label
          + '<span>'
          + drupalSettings.ageGateSettings.language_texts.languages[currentLang].remember_warning
          + '</span>'
          + '</label>'
          + '</div>'
          + '<button type="submit" style="background-color: ' + buttonBgColor + '; color: ' + buttonTextColor + '" class="agegate-submit ' + hideInputsClass + '" tabindex="6">' + drupalSettings.ageGateSettings.language_texts.languages[currentLang].button_continue + '</button>'
          + '</form>' // End of input form
          + '<div class="footer-overlay">'
          + '<p style="color: ' + fontColor + '">' + drupalSettings.ageGateSettings.language_texts.languages[currentLang].shortimprint + '</p>';
        if (ageGateType !== '2') {
          html += '<p style="color: ' + fontColor + '">' + drupalSettings.ageGateSettings.language_texts.languages[currentLang].responsibledrinking + '</p>';
        }
        html += '<p style="color: ' + fontColor + '">' + drupalSettings.ageGateSettings.language_texts.languages[currentLang].contentsharing + '</p>'
          + '</div>'
          + '</div>'
          + '</div>';
        html = $(html);
        html.prependTo(document.body);

        $('.dynamical-wrapper .year').on('change', function () {
          sanitizeValue(this, 4);
          let year = $(this).val();
          let month = 12;
          let day = 31;
          let years = _calculateAge(new Date(year + '-' + month + '-' + day));
          if (years === 17) {
            $('.dynamical-wrapper .month').removeClass('visually-hidden').focus();
          }
          if (years > 17) {
            $('.dynamical-wrapper .day').addClass('visually-hidden');
            $('.dynamical-wrapper .month').addClass('visually-hidden');
          }
        });

        $('.dynamical-wrapper .month').on('change', function () {
          sanitizeValue(this, 2);
          let year = $('.dynamical-wrapper .year').val();
          let month = +$(this).val();
          let day = new Date(year, month + 1, 0).getDate();
          let currentMonth = new Date().getMonth() + 1;
          let years = _calculateAge(new Date(year + '-' + month + '-' + day));

          if (years === 17 && currentMonth === month) {
            $('.dynamical-wrapper .day').removeClass('visually-hidden').focus();

          }
        });

        $('.dynamical-wrapper .day').on('change', function () {
          sanitizeValue(this, 2);
          let year = $('.dynamical-wrapper .year').val();
          let month = $('.dynamical-wrapper .month').val();
          let day = $(this).val();

          let years = _calculateAge(new Date(year + '-' + month + '-' + day));
        });

        let sanitizeValue = function (inputElement, maxLength) {
          inputElement.value = inputElement.value.replace(/[^0-9]/, '').substring(0, maxLength);
        };

        let setEvents = function () {
          var inputYear = $('.year');
          var inputMonth = $('.month');
          var inputDay = $('.day');

          inputDay.keyup(function (e) {
            sanitizeValue(this, 2);
            if (this.value.length === 2 && !isNaN(parseInt(e.key))) {
              this.blur();
              inputMonth.focus();
            }
          });

          inputMonth.keyup(function (e) {
            sanitizeValue(this, 2);
            if (this.value.length === 2 && !isNaN(parseInt(e.key))) {
              this.blur();
              inputYear.focus();
            }
          });

          inputMonth.keydown(function (e) {
            if (this.value.length === 0 && e.keyCode === 8) {
              this.blur();
              inputDay.focus();
            }
          });

          inputYear.keydown(function (e) {
            if (this.value.length === 0 && e.keyCode === 8) {
              this.blur();
              inputMonth.focus();
            }
          });

          inputYear.keyup(function (e) {
            sanitizeValue(this, 4);
            if (inputDay.val() === 0 || inputMonth.val() === 0 || inputYear.val() === 0) {
              return;
            }

            if (this.value.length === 4 && !isNaN(parseInt(e.key))) {
              $('.agegate-submit').focus();
            }
          });
          inputDay.focus();

          // Yes/No Agegate
          $('.buttons-wrapper input').on('click', function () {
            if ($(this).hasClass('yes')) {
              cookies.set('agegate', 1);
              $('#age-overlay').hide();
              DatalayerPositiveAgeGate();
              if (langPreselect) {
                setRedirectToLanguage();
              }
            }
            if ($(this).hasClass('no')) {
              $('.agegate-error-too-young').show();
              $('.buttons-wrapper').hide();
              DatalayerNegativeAgeGate();
            }
          });
        };

        $('input.day').on('focus', () => {
          focusInput();
        });
        $('input.month').on('focus', () => {
          focusInput();
        });
        $('input.year').on('focus', () => {
          focusInput();
        });

        setEvents();

        $('form').submit(function (e) {
          e.preventDefault();
          getInputAge();
        });

        function focusInput() {
          $('.agegate-error-invalid-dob').hide();
          $('.agegate-error-too-young').hide();

          $('.day').removeClass('error');
          $('.month').removeClass('error');
          $('.year').removeClass('error');
        }

        function getInputAge() {
          let errors = false;
          let day = $('.day').val();
          let month = $('.month').val();
          let year = $('.year').val();

          if (ageGateType === '3') {
            day = day !== '' ? day : 31;
            month = month !== '' ? month : 12;
          }

          if (day === '' || day < 1 || day > 31 || !parseInt(day)) {
            $('.day').addClass('error');
            $('.agegate-error-invalid-dob').show();
            errors = true;
            DatalayerNegativeAgeGate();
          }

          if (month === '' || month < 1 || month > 12 || !parseInt(month)) {
            $('.month').addClass('error');
            $('.agegate-error-invalid-dob').show();
            errors = true;
            DatalayerNegativeAgeGate();
          }

          if (year === '' || year < 1900 || year > new Date().getUTCFullYear() || !parseInt(year)) {
            $('.year').addClass('error');
            $('.agegate-error-invalid-dob').show();
            errors = true;
            $.post('/ab_age_gate/update', { 'type': 'fail', 'isMobile': +isMobile }, function (e) {
            });
            DatalayerNegativeAgeGate();
          }

          if (day !== '' && month !== '' && year !== '' && !errors) {
            let is_adult = isDate18orMoreYearsOld(day, month, year);

            if (!is_adult) {
              $('.agegate-error-too-young').show();
              $('.input-wrapper').hide();
              $('button.agegate-submit').css('visibility', 'hidden');
              $('.agegate-remember').css('visibility', 'hidden');
              $.post('/ab_age_gate/update', { 'type': 'under18', 'isMobile': +isMobile }, function (e) {
              });
              DatalayerNegativeAgeGate();
            } else {
              DatalayerPositiveAgeGate();

              if ($('input[name="agegate-remember-item"]').is(':checked')) {
                cookies.set('agegate', '1', { expires: null, path: '/' });
              } else {
                cookies.set('agegate', 1);
              }
              if (langPreselect) {
                setRedirectToLanguage();
              } else {
                $('#age-overlay').hide();
              }
              $.post('/ab_age_gate/update', { 'type': 'success', 'isMobile': +isMobile }, function (e) {
              });

            }
          }
        }

        var $lang = '';

        $('.step-1 a').on('click', function () {
          $('.step-1 a').removeClass('active');
          $(this).addClass('active');
          let selectedLanguage = $(this).data('lang');
          if ($(this).data('lang') === defaultLanguage) {
            selectedLanguage = '';
          }
          $lang = selectedLanguage;
          $('form.input-container').data('lang', selectedLanguage);
        });

        $('.step-1 .step1-submit').on('click', function () {
          $('.overlay-header-text.step-1').addClass('hidden');
          $('.overlay-header-text.step-2').removeClass('hidden');
          $('.input-container').removeClass('hidden');
        });

        function setRedirectToLanguage() {
          var urlData = window.location.pathname.split('/');
          urlData.splice(0, 2).join('/');
          var newPath = urlData.join('/');
          let selected_lang = $('.input-container').data('lang');
          let newUrl = '/';
          if (selected_lang !== undefined && selected_lang.length) {
            newUrl = '/' + selected_lang + '/' + newPath;
          } else {
            newUrl = '/' + newPath;
          }
          window.location.href = newUrl;
          return newUrl;
        }

        $('input[name="agegate-remember-item"]').on('click', function () {
          if ($(this).is(':checked')) {
            if (useDatalayer) {
              dataLayer.push({
                'event': 'GAEvent',
                'event_category': 'Age Gate',
                'event_action': 'Interaction',
                'event_label': 'Remember Me',
                'interaction': 'False',
                'component_name': 'remember_me', // example:button_seemore
                'element_text': 'remember_me', // example: submit
              });
            }
          }
        });

        $.get('/ab_age_gate/select', function (data) {
          if (data === '' || data === false || data === 'false') {
            $.post('/ab_age_gate/insert', { 'load': 1, 'isMobile': +isMobile }, function (e) {
            });
          } else {
            $.post('/ab_age_gate/update', { 'type': 'load', 'isMobile': +isMobile }, function (e) {
            });
          }
        });
      });

      function isDate18orMoreYearsOld(day, month, year) {
        return new Date(+year + +18, +month - 1, +day) <= new Date();
      }

      function _calculateAge(birthday) { // birthday is a date
        var now = new Date();
        var current_year = now.getFullYear();
        var year_diff = current_year - birthday.getFullYear();
        var birthday_this_year = new Date(current_year, birthday.getMonth(), birthday.getDate());
        var has_had_birthday_this_year = (now >= birthday_this_year);

        return has_had_birthday_this_year
          ? year_diff
          : year_diff - 1;
      }

      // Positive agegate
      function DatalayerPositiveAgeGate() {
        if (useDatalayer) {
          dataLayer.push({
            'event': 'GAEvent',
            'event_category': 'Age Gate',
            'event_action': 'Interaction',
            'event_label': 'Yes',
            'interaction': 'False',
            'component_name': 'agegate_submit',
            'element_text': 'Agegate Submit',
            //'user_email_phone_hash': phone_hash
          });
        }
      }

      // Negative agegate
      function DatalayerNegativeAgeGate() {
        if (useDatalayer) {
          dataLayer.push({
            'event': 'GAEvent',
            'event_category': 'Age Gate',
            'event_action': 'Interaction',
            'event_label': 'No',
            'interaction': 'False',
            'component_name': 'agegate_submit',
            'element_text': 'Agegate Submit',
            //'user_id_phone_hash': phone_hash
          });
        }
      }
    },
  };
})(jQuery, Drupal, window.Cookies, drupalSettings);
